var count = 0;
var el = document.querySelector('.odometer');

window.odometerOptions = {
  auto: false,
  selector: '.odometer',
  duration: 300,
  theme: 'car',
  animation: 'count'
};

document.getElementById("add").addEventListener('click', function() {
  count==9999?count=0:count++;
  el.innerHTML = count;
});

document.getElementById("reset").addEventListener('click', function() {
  count=0;
  el.innerHTML = count;
});

document.addEventListener("keydown", function (e) {
  if (e.which === 123) {
    require('electron').remote.getCurrentWindow().toggleDevTools();
  } else if (e.which === 116) {
    location.reload();
  }
});